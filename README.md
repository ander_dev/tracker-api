#Tracker API

Tracker-api is written in springboot using jpa, mariaDB putting all together with docker-compose

Application written as per request, basic rules user can only post its Mood once a day

NOTE 1 - DB is persisted to local file system to prevent us to have to create data every time we compose it.

#Tracker WEB

Tracker-web was written in Angular 13, using modules BrowserModule, HttpClientModule, FormsModule, ReactiveFormsModule, to design a pretty basic application that allow user to choose a mood and submit to be saved by the API.

My lac of knowledge on the UI side make very difficult to display the chart of “Overall Team Mood” but I am displaying the totals of it in a table, I couldn't really find a easy library to implement the chars.

NOTE 1 - when user access the landing page, UI creates a cookie where I save an uuid, which later it will be submitted, this was implemented to control same user trying to post moods in less than 24hrs.

NOTE 2 - for test purpose, this cook only leaves for 2 minutes, for us to test saving user_moods at the same tab every 2 minutes, otherwise, trying to save a second mood in less than 2 minutes system will block at user will fall in the 24hr rule.






#Row to run
###pre-requirements
* docker

###steps
* navigate to the mood-tracker folder
* run ```docker-compose up --build```

this will bring up DB, API and WEB where to access the application use http://localhost:4201