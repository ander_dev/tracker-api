package com.tracker.entity;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

@MappedSuperclass
public class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "uuid2")
    @GenericGenerator(
            name = "uuid2",
            strategy = "uuid2"
    )
    @Column(insertable = false)
    @Type(type="uuid-char")
    protected UUID id;

    @Column(name = "created_on",updatable = false)
    protected LocalDateTime createdOn;

    @Column(name = "updated_on")
    protected LocalDateTime updatedOn;

    @PrePersist
    public void onSave() {
        createdOn = LocalDateTime.now(ZoneOffset.UTC);
        updatedOn = LocalDateTime.now(ZoneOffset.UTC);
    }

    @PreUpdate
    public void onUpdate() {
        updatedOn = LocalDateTime.now(ZoneOffset.UTC);
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

}
