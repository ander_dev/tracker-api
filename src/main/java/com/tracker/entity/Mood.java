package com.tracker.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mood",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"description"}, name = "unique_mood_description")
        },
        indexes = {
                @Index(columnList = "description", name = "idx_mood_description")
        }
)
public class Mood {
    @Id
    private Integer id;

    @Column(updatable = false)
    private String description;

    @Column(name = "mood_order",updatable = false)
    private Integer moodOrder;

    @Column(name = "created_on",updatable = false)
    protected LocalDateTime createdOn;

    public String getDescription() {
        return description;
    }

    public Integer getMoodOrder() {
        return moodOrder;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    @Override
    public String toString() {
        return "Mood{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", moodOrder=" + moodOrder +
                ", createdOn=" + createdOn +
                '}';
    }
}
