package com.tracker.entity.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import javax.validation.constraints.Size;

import java.util.UUID;

@JsonRootName("user_mood")
public class UserMoodDTO {

    @JsonProperty("user_id")
    protected UUID userId;
    private String mood;
    @Size(max=50, message = "Message cannot be bigger than 350 characters!")
    private String message;

    public UserMoodDTO(UUID userId, String mood, String message) {
        this.userId = userId;
        this.mood = mood;
        this.message = message;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
