package com.tracker.entity.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("user_mood_totals")
public class UserMoodTotalsDTO {

    private String description;
    private Integer count;

    public UserMoodTotalsDTO(String description, Integer count) {
        this.description = description;
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public Integer getCount() {
        return count;
    }
}
