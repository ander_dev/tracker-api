package com.tracker.entity.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.tracker.entity.UserMood;

import java.util.Date;

@JsonRootName("response")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDTO {

    private Long timestamp;

    @JsonProperty("status_code")
    private int statusCode;

    @JsonProperty("user_mood")
    private UserMood userMood;

    private String messages;

    public ResponseDTO() {
        //default constructor for deserialization
    }

    public ResponseDTO(String messages, int statusCode) {
        this.timestamp = new Date().getTime() / 1000;;
        this.statusCode = statusCode;
        this.messages = messages;
    }
    public ResponseDTO(UserMood userMood, String messages, int statusCode) {
        this.timestamp = new Date().getTime() / 1000;;
        this.statusCode = statusCode;
        this.userMood = userMood;
        this.messages = messages;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public UserMood getUserMood() {
        return userMood;
    }

    public String getMessages() {
        return messages;
    }
}
