package com.tracker.entity.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("mood")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MoodDTO {
    private Integer id;

    private String description;

    @JsonProperty("mood_order")
    private Integer moodOrder;

    public MoodDTO(String description, Integer moodOrder) {
        this.description = description;
        this.moodOrder = moodOrder;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Integer getMoodOrder() {
        return moodOrder;
    }
}
