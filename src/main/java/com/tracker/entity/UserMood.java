package com.tracker.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "user_mood")
public class UserMood extends BaseEntity{

    @Type(type="uuid-char")
    @Column(name = "user_id")
    protected UUID userId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "mood_id", foreignKey = @ForeignKey(name = "fk_user_mood_mood_id"))
    private Mood mood;

    private String message;

    public UserMood() {
        //default constructor to make JPA happy!
    }

    public UserMood(UUID userId, Mood mood, String message) {
        this.userId = userId;
        this.mood = mood;
        this.message = message;
    }

    public Mood getMood() {
        return mood;
    }

    public String getMessage() {
        return message;
    }

    public UUID getUserId() {
        return userId;
    }
}
