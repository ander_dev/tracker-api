package com.tracker.repository;

import com.tracker.entity.Mood;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MoodRepository extends JpaRepository<Mood, Integer> {

    List<Mood> findAllByOrderByMoodOrder();

    Optional<Mood> findByDescription(String description);

}
