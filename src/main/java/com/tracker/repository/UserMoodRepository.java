package com.tracker.repository;

import com.tracker.entity.UserMood;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserMoodRepository extends JpaRepository<UserMood, UUID> {

    List<UserMood> findAll();

    UserMood findFirstByUserIdOrderByCreatedOnDesc(UUID userId);

}
