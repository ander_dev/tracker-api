package com.tracker.controller;

import com.tracker.entity.dto.MoodDTO;
import com.tracker.entity.dto.ResponseDTO;
import com.tracker.entity.dto.UserMoodDTO;
import com.tracker.entity.dto.UserMoodTotalsDTO;
import com.tracker.service.MoodService;
import com.tracker.service.UserMoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/mood_tracker")
public class MoodController {

    private final UserMoodService userMoodService;
    private final MoodService moodService;

    @Autowired
    public MoodController(UserMoodService userMoodService, MoodService moodService) {
        this.userMoodService = userMoodService;
        this.moodService = moodService;
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseDTO> createUserMood(@Valid @RequestBody UserMoodDTO userMoodDTO) {
        ResponseDTO response = userMoodService.save(userMoodDTO);
        return ResponseEntity.status(response.getStatusCode()).body(response);
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<UserMoodTotalsDTO>> getMoodTotals() {
        List<UserMoodTotalsDTO> results = userMoodService.getUserMoodTotals();
        return ResponseEntity.status(200).body(results);
    }

    @GetMapping(path = "/moods", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<MoodDTO>> getMoods() {
        List<MoodDTO> results = moodService.getListOrdered();
        return ResponseEntity.status(200).body(results);
    }
}
