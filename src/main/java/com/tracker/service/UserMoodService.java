package com.tracker.service;

import com.tracker.entity.UserMood;
import com.tracker.entity.dto.ResponseDTO;
import com.tracker.entity.dto.UserMoodDTO;
import com.tracker.entity.dto.UserMoodTotalsDTO;

import java.util.List;
import java.util.UUID;

public interface UserMoodService {

    List<UserMood> list();

    ResponseDTO save(UserMoodDTO userMoodDTO);

    List<UserMoodTotalsDTO> getUserMoodTotals();

    void delete(UUID userMoodId);
}
