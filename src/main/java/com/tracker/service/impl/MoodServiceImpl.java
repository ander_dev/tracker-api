package com.tracker.service.impl;

import com.tracker.entity.Mood;
import com.tracker.entity.dto.MoodDTO;
import com.tracker.repository.MoodRepository;
import com.tracker.service.MoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MoodServiceImpl implements MoodService {

    private final MoodRepository moodRepository;

    @Autowired
    public MoodServiceImpl(MoodRepository moodRepository) {
        this.moodRepository = moodRepository;
    }

    @Override
    public List<MoodDTO> getListOrdered() {

        List<Mood> moods = moodRepository.findAllByOrderByMoodOrder();
        return moods.stream().map(x-> new MoodDTO(x.getDescription(), x.getMoodOrder())).collect(Collectors.toList());
    }

    @Override
    public Optional<Mood> findByDescription(String description) {
        return moodRepository.findByDescription(description);
    }
}
