package com.tracker.service.impl;

import com.tracker.entity.Mood;
import com.tracker.entity.UserMood;
import com.tracker.entity.dto.ResponseDTO;
import com.tracker.entity.dto.UserMoodDTO;
import com.tracker.entity.dto.UserMoodTotalsDTO;
import com.tracker.exception.CustomException;
import com.tracker.repository.MoodRepository;
import com.tracker.repository.UserMoodRepository;
import com.tracker.service.UserMoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserMoodServiceImpl implements UserMoodService {

    private final UserMoodRepository userMoodRepository;
    private final MoodRepository moodRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public UserMoodServiceImpl(UserMoodRepository userMoodRepository, MoodRepository moodRepository) {
        this.userMoodRepository = userMoodRepository;
        this.moodRepository = moodRepository;
    }

    @Override
    public List<UserMood> list() {
        return userMoodRepository.findAll();
    }

    @Override
    @Transactional
    public ResponseDTO save(UserMoodDTO userMoodDTO) {
        Mood mood = moodRepository.findByDescription(userMoodDTO.getMood()).orElseThrow(() -> new CustomException(String.format("Mood description %s, not found!", userMoodDTO.getMood()), HttpStatus.NOT_FOUND));

        UserMood userMood = userMoodRepository.findFirstByUserIdOrderByCreatedOnDesc(userMoodDTO.getUserId());

        if (userMood != null && LocalDateTime.now(ZoneOffset.UTC).isBefore(userMood.getCreatedOn().plus(24, ChronoUnit.HOURS))) {
            throw new CustomException("Sorry, you have already submitted your response for today, try again tomorrow!", HttpStatus.CONFLICT);
        }

        userMood = new UserMood(userMoodDTO.getUserId(), mood, userMoodDTO.getMessage());
        UserMood saved = userMoodRepository.save(userMood);
        return new ResponseDTO(saved, "Your mood has been successfully submitted.", HttpStatus.CREATED.value());
    }

    @Override
    public List<UserMoodTotalsDTO> getUserMoodTotals() {
        List<Object[]> results = entityManager
                .createNativeQuery("select m.description, count(um.mood_id) count " +
                        "from user_mood um " +
                        "         join mood m on m.id = um.mood_id " +
                        "         and Date(um.created_on) = Date(now())"+
                        "group by m.description")
                .getResultList();
        List<UserMoodTotalsDTO> totals = new ArrayList<>();
        for (Object[] result : results) {
            totals.add(new UserMoodTotalsDTO(result[0].toString(), Integer.valueOf(result[1].toString())));
        }
        return totals;
    }

    @Override
    public void delete(UUID userMoodId) {
        userMoodRepository.deleteById(userMoodId);
    }
}
