package com.tracker.service;

import com.tracker.entity.Mood;
import com.tracker.entity.dto.MoodDTO;

import java.util.List;
import java.util.Optional;

public interface MoodService {

    List<MoodDTO> getListOrdered();

    Optional<Mood> findByDescription(String description);
}
