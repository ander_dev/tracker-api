package com.tracker.exception;

import com.tracker.entity.dto.ResponseDTO;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

/**
 * CustomGlobalExceptionHandler controls exceptions thrown by Spring, where than we manipulate this response throwing back a readable payload to the UI
 */
@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<Object> customHandlingException(CustomException ex) {
        ResponseDTO responseDTO = new ResponseDTO(ex.getMessage(), ex.getStatus().value());
        return ResponseEntity.status(ex.getStatus().value()).body(responseDTO);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = ex.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList()).get(0);
        ResponseDTO responseDTO = new ResponseDTO(error, status.value());
        return new ResponseEntity<>(responseDTO, headers, status);

    }
}
