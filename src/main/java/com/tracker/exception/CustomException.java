package com.tracker.exception;

import org.springframework.http.HttpStatus;

/**
 * CustomsException was created to be able to manipulate payload when throwing exception back to UI
 */
public class CustomException extends RuntimeException {

    private String message;

    private HttpStatus status;

    public CustomException(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
