insert into mood(id, description, mood_order) values (1, 'Happy', 1);
insert into mood(id, description, mood_order) values (2, 'Just normal really', 2);
insert into mood(id, description, mood_order) values (3, 'A bit “meh”', 3);
insert into mood(id, description, mood_order) values (4, 'Grumpy', 4);
insert into mood(id, description, mood_order) values (5, 'Stressed out – not a happy camper', 5);