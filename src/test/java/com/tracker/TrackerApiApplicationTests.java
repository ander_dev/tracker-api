package com.tracker;

import com.tracker.entity.Mood;
import com.tracker.entity.dto.MoodDTO;
import com.tracker.entity.dto.ResponseDTO;
import com.tracker.entity.dto.UserMoodDTO;
import com.tracker.exception.CustomException;
import com.tracker.service.MoodService;
import com.tracker.service.UserMoodService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TrackerApiApplication.class}, loader = SpringBootContextLoader.class)
class TrackerApiApplicationTests {

	private final MoodService moodService;
	private final UserMoodService userMoodService;
	private UUID userMoodId;

	@Autowired
	TrackerApiApplicationTests(MoodService moodService, UserMoodService userMoodService) {
		this.moodService = moodService;
		this.userMoodService = userMoodService;
	}

	@Test
	void testListAllMoods() {
		List<MoodDTO> listAll = moodService.getListOrdered();
		assertFalse(listAll.isEmpty());
	}

	@Test
	void testGetMoodByDescription() {
		Optional<Mood> optionalMood = moodService.findByDescription("Happy");
		assertTrue(optionalMood.isPresent());
		assertEquals("Happy", optionalMood.get().getDescription());
	}

	@Test
	void testSaveUserMood() {
		UUID userId = UUID.randomUUID();
		UserMoodDTO userMoodDTO = new UserMoodDTO(userId, "Happy", "Very happy today!");
		ResponseDTO responseDTO = userMoodService.save(userMoodDTO);
		this.userMoodId = responseDTO.getUserMood().getId();
		assertEquals("Happy", responseDTO.getUserMood().getMood().getDescription());
		assertEquals("Very happy today!", responseDTO.getUserMood().getMessage());
		assertEquals(userId, responseDTO.getUserMood().getUserId());
		assertEquals("Your mood has been successfully submitted.", responseDTO.getMessages());
	}

	@Test()
	void testSaveUserMoodNonExistentMood() {
		UUID userId = UUID.randomUUID();
		UserMoodDTO userMoodDTO = new UserMoodDTO(userId, "Happiest", "Very happy today!");
		Exception exception = assertThrows(CustomException.class, () -> userMoodService.save(userMoodDTO));
		String error = "Mood description Happiest, not found!";
		String errorMessage = exception.getMessage();
		assertTrue(errorMessage.contains(error));
	}

	@Test()
	void testSaveUserMoodInLessThan24hrs() {
		UUID userId = UUID.randomUUID();
		UserMoodDTO userMoodDTO = new UserMoodDTO(userId, "Happy", "Very happy today!");
		ResponseDTO responseDTO = userMoodService.save(userMoodDTO);
		this.userMoodId = responseDTO.getUserMood().getId();

		assertEquals("Your mood has been successfully submitted.", responseDTO.getMessages());

		UserMoodDTO newUserMoodDTO = new UserMoodDTO(userId, "Just normal really", "Just normal really");
		Exception exception = assertThrows(CustomException.class, () -> userMoodService.save(newUserMoodDTO));
		String error = "Sorry, you have already submitted your response for today, try again tomorrow!";
		String errorMessage = exception.getMessage();
		assertTrue(errorMessage.contains(error));

	}

	@AfterEach
	void deleteUserMood(){
		if(userMoodId != null) {
			userMoodService.delete(userMoodId);
		}
	}

}
